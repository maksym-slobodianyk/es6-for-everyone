import {createElement} from "../helpers/domHelper";
import {showModal} from "./modal";

export function showWinnerModal(fighter) {
    const title = 'Winner of the battle';
    const bodyElement = createWinner(fighter);
    showModal({title, bodyElement});
}

function createWinner(fighter) {
    const {name, source} = fighter;
    const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
    const nameElement = createElement({tagName: 'span', className: 'modal-detail-name'});
    const imgContainerElement = createElement({tagName: 'div', className: 'fighter-image-container'});
    const imgElement = createElement({tagName: 'img', className: 'fighter-image'});
    imgContainerElement.append(imgElement);
    nameElement.innerText = name;
    imgElement.src = source;
    fighterDetails.append(imgContainerElement);
    fighterDetails.append(nameElement);
    return fighterDetails;
}
