import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const {name, health, attack, defense,source} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({tagName: 'span', className: 'modal-detail-name'});
  const defenseElement = createElement({tagName: 'span', className: 'modal-detail'});
  const healthElement = createElement({tagName: 'span', className: 'modal-detail'});
  const attackElement = createElement({tagName: 'span', className: 'modal-detail'});
  const imgContainerElement = createElement({tagName: 'div', className: 'fighter-image-container'});
  const imgElement = createElement({tagName: 'img', className: 'fighter-image'});


  nameElement.innerText = name;
  healthElement.innerText = `❤: ${health} points`;
  attackElement.innerText = `⚔: ${attack} points`;
  defenseElement.innerText = `🛡: ${defense} points`;
  imgElement.src = source;
  imgContainerElement.append(imgElement);
  fighterDetails.append(imgContainerElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);

  return fighterDetails;
}
