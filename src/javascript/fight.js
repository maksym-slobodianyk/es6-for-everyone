export function fight(firstFighter, secondFighter) {
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    while (true) {
        if ((firstFighterHealth -= getDamage(secondFighter, firstFighter)) <= 0) {
            return secondFighter;
        }
        if ((secondFighterHealth -= getDamage(firstFighter, secondFighter)) <= 0) {
            return firstFighter;
        }

    }
}

export function getDamage(attacker, enemy) {
    const damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
    const criticalHitChance = randomInt(1, 2);
    return criticalHitChance * fighter.attack;
}

export function getBlockPower(fighter) {
    const dodgeChance = randomInt(1, 2);
    return dodgeChance * fighter.defense;
}

function randomInt(min, max) {
    return min + (max - min) * Math.random();
}
